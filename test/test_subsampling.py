import numpy

from cell_typer.utils import subsample


cell_transcript_UMI_counts = [
    {
        "Transcript_1": [5, 15, 22],
        "Transcript_2": [7, 1, 1, 1],
        "Transcript_3": [3]
    },
    {
        "Transcript_1": [1, 2, 1],
        "Transcript_3": [1]
    },
    {
        "Transcript_1": [2, 55, 33]
    }
]

subsampled_reads = subsample.subsample_reads(
    cell_transcript_UMI_counts,
    0.5,
    subsample_method=subsample.Subsample_Method.DETERMINISTIC
)

theoretical_subsampled_reads = [
    {
        "Transcript_1": [2, 7, 11],
        "Transcript_2": [3],
        "Transcript_3": [1]
    },
    {
        "Transcript_1": [1]
    },
    {
        "Transcript_1": [1, 27, 16]
    }
]

if subsampled_reads != theoretical_subsampled_reads:
    raise ValueError("Stupid subsampling failed!")

cell_gene_counts = numpy.array(
    [
        [5, 5, 10],
        [2, 1, 0]
    ]
)

theoretical_subsampled_gene_counts = numpy.array(
    [
        [2, 2, 5],
        [1, 0, 0]
    ]
)

subsampled_gene_counts = subsample.subsample_cell_gene_counts(
    cell_gene_counts,
    subsample_factor=0.5,
    subsample_method=subsample.Subsample_Method.DETERMINISTIC
)

if not numpy.array_equal(
        theoretical_subsampled_gene_counts,
        subsampled_gene_counts):
    raise ValueError("Stupid gene count subsampling failed!")

subsampled_gene_counts = numpy.zeros(cell_gene_counts.shape)

for _ in range(1000):
    subsampled_gene_counts += subsample.subsample_cell_gene_counts(
        cell_gene_counts,
        subsample_factor=0.5,
        subsample_method=subsample.Subsample_Method.UNIFORM_RANDOM)

subsampled_gene_counts /= 1000

if not numpy.allclose(theoretical_subsampled_gene_counts,
        subsampled_gene_counts, rtol=1):
    raise ValueError("Uniform random gene count subsampling failed!")

print("Test successful")
