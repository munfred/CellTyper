from enum import Enum
import numpy
import math


class Subsample_Method(Enum):

    DETERMINISTIC = 1
    UNIFORM_RANDOM = 2


def subsample_cell_gene_counts(
        cell_gene_counts,
        subsample_factor,
        subsample_method=Subsample_Method.DETERMINISTIC):
    """
    Takes a 2D array of cells x genes and subsamples it by the given subsample
    factor

    Required Arguments
    ------------------
    cell_gene_counts : numpy.ndarray
        A 2D array of cell gene counts, where each row is a cell and each column
        is a gene
    subsample_factor : float (0, 1)
        How much to reduce the dataset size by

    Optional Arguments
    ------------------
    subsample_method : Subsample_Method
        Which method to use for subsampling, see Subsample_Method

    Returns
    -------
    numpy.ndarray
        Same format as the cell_gene_counts, but subsampled. Indices of cells
        and genes are maintained
    """

    if subsample_factor <= 0 or subsample_factor >= 1:
        raise ValueError("Subsample factor should be on the interval (0, 1)")

    # Get the counts for each cell, we will subsample that first
    cell_counts = cell_gene_counts.sum(axis=1)

    num_cells = cell_gene_counts.shape[0]
    num_genes = cell_gene_counts.shape[1]

    # To get subsampled cell counts, we just use a fixed subsample factor -
    # this guarantees the cell counts will have close to the same distribution.
    # Noise will come from the gene sampling
    subsampled_cell_counts = \
        numpy.round(cell_counts * subsample_factor).astype(numpy.uint32)

    subsampled_cell_gene_counts = numpy.zeros(cell_gene_counts.shape)

    if subsample_method == Subsample_Method.UNIFORM_RANDOM:

        for cell_index in range(num_cells):
            gene_counts = cell_gene_counts[cell_index, :]
            gene_probabilities = gene_counts / gene_counts.sum()

            sampled_gene_indices = numpy.random.choice(
                list(range(num_genes)),
                size=subsampled_cell_counts[cell_index],
                p=gene_probabilities,
                replace=True
            )

            for sampled_gene_index in sampled_gene_indices:
                subsampled_cell_gene_counts[cell_index, sampled_gene_index] += 1

    elif subsample_method == Subsample_Method.DETERMINISTIC:
        subsampled_cell_gene_counts = \
            numpy.floor(cell_gene_counts * subsample_factor)\
                .astype(numpy.uint32)

    else:
        raise NotImplementedError("Only have deterministic and uniform sampler "
                                  "implemented")

    return subsampled_cell_gene_counts


def subsample_reads(
        cell_transcript_UMI_counts,
        subsample_factor,
        subsample_method=Subsample_Method.DETERMINISTIC):
    """
    Takes a cell transcript UMI object and subsamples it to simulate lower read
    depths

    Required Arguments
    ------------------
    cell_transcript_UMI_counts : list of dictionaries of lists
        A list of cell reads, where each entry is a dictionary of transcript
        names and their UMI counts.
        e.g. [
                {
                    "Transcript_1": [5, 15, 1],
                    "Transcript_2": [],
                    ...
                },
                {
                    "Transcript_1": [],
                    ...
                }
                ...
            ]
    subsample_factor : float (0, 1)
        How much to reduce the dataset size by

    Optional Arguments
    ------------------
    subsample_method : Subsample_Method
        Which method to use for subsampling, see Subsample_Method

    Returns
    -------
    list of dictionaries of lists
        Same format as the cell_transcript_UMI_counts, but subsampled. Indices
        of cells are maintained
    """

    if subsample_factor <= 0 or subsample_factor >= 1:
        raise ValueError("subsample_factor must be between 0 and 1 (exclusive)")

    subsampled_cell_transcript_UMI_counts = []

    if subsample_method == Subsample_Method.DETERMINISTIC:

        for cell in cell_transcript_UMI_counts:

            subsampled_transcript_UMI_counts = {}

            for transcript_name, transcript_UMI_counts in cell.items():

                subsampled_UMI_counts = []

                for UMI_count in transcript_UMI_counts:

                    subsampled_UMI_count = math.floor(UMI_count *
                                                      subsample_factor)

                    if subsampled_UMI_count == 0:
                        continue

                    subsampled_UMI_counts.append(subsampled_UMI_count)

                if len(subsampled_UMI_counts) == 0:
                    continue

                subsampled_transcript_UMI_counts[transcript_name] = \
                    subsampled_UMI_counts

            subsampled_cell_transcript_UMI_counts.append(
                subsampled_transcript_UMI_counts)

    elif subsample_method == Subsample_Method.UNIFORM_RANDOM:

        cell_transcript_UMI_weights = []
        cell_transcript_names = []
        cell_start_indices = []

        cell_transcript_UMI_index = 0

        for transcript_UMI_counts in cell_transcript_UMI_counts:

            cell_start_indices.append(cell_transcript_UMI_index)

            for transcript_name, UMI_counts in transcript_UMI_counts.items():

                for UMI_count in UMI_counts:
                    cell_transcript_UMI_weights.append(UMI_count)
                    cell_transcript_names.append(transcript_name)
                    cell_transcript_UMI_index += 1

        cell_start_indices.append(cell_transcript_UMI_index)

        cell_transcript_UMI_weights = numpy.array(cell_transcript_UMI_weights)

        total_reads = cell_transcript_UMI_weights.sum()
        cell_transcript_UMI_weights = cell_transcript_UMI_weights / total_reads
        num_subsampled_reads = int(round(total_reads * subsample_factor))

        subsampled_transcript_indices = numpy.random.choice(
            list(range(cell_transcript_UMI_weights.shape[0])),
            size=num_subsampled_reads,
            replace=True,
            p=cell_transcript_UMI_weights
        )

        raise NotImplementedError("Nevermind I didn't finish this function")

    else:
        raise NotImplementedError("Subsample method not implemented")

    return subsampled_cell_transcript_UMI_counts
